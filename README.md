# Noiz2sa

This is an abstract Shoot 'em up game. It's originally written by Kenta Cho.

This is a modification of the original game available
[here](http://www.asahi-net.or.jp/~cs8k-cyu/windows/noiz2sa_e.html).


## How to build

This game uses [Meson](http://mesonbuild.com/) which requires
[Ninja](https://ninja-build.org/) and [Python](https://www.python.org/). You
also need a C/C++ compiler with the following libraries:

- [SDL 2.0](https://www.libsdl.org/)
- [SDL_mixer 2.0](https://www.libsdl.org/projects/SDL_mixer/)

If you want to run the game from this folder:

    $ meson setup build --buildtype=release -Dlocaldata=true
    $ ninja -v -C build
    $ build/src/noiz2sa

or if you want to package this:

    $ meson setup build --buildtype=plain --prefix=$prefix
    $ ninja -v -C build
    $ DESTDIR=$pkgdir ninja -v -C build install


## How to play

|Function|Keyboard|Joystick|
|---|---|---|
|Movement|Arrow keys|Joystick|
|Fire|Z|Button 1/4|
|Slowdown|X|Button 2/3|
|Pause|P||

Select a stage and press the fire button to start a game.

Control your ship and avoid the barrage.
A ship is not destroyed even if it contacts an enemy main body.
A ship becomes slow while holding the slowdown key.

A green star is the bonus item.
A score of the item(displayed at the left-up corner) increases if you get items
continuously.

When all ships are destroyed, the game is over.
The ship extends 200,000 and every 500,000 points.


## Command line options

    --nosound       Disable sound
    --window        Launch the game in the window mode
    --reverse       Reverse the fire key and the slowdown key
    --brightness n  Set the brightness of the sceen (n=0..256)
    --accframe      Use the alternative framerate management algorithm
                    (If you have a problem with framerate, try this option)
    --nearest       User nearest scaling instead of linear scaling


## Screenshots

![](shot0.png)
![](shot1.png)
![](shot2.png)
![](shot3.png)

---

Noiz2sa is BSD (c) Kenta Cho

Modifications are (c) Christian Buschau
